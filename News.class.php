<?php
    class News extends Publication{
        protected $source;

        public function __construct($type, $title, $introduction, $full_text, $source){
            parent::__construct($type, $title, $introduction, $full_text);
            $this->source = $source;
        }

        public function getSource(){
            return $this->source;
        }
    }