<?php
    class Article extends Publication{
        protected $author;

        public function __construct($type, $title, $introduction, $full_text, $author){
            parent::__construct($type, $title, $introduction, $full_text);
            $this->author = $author;
        }

        public function getAuthor(){
            return $this->author;
        }
    }