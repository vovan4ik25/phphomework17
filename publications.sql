-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Июн 06 2017 г., 23:47
-- Версия сервера: 5.7.16
-- Версия PHP: 7.1.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `publications`
--

-- --------------------------------------------------------

--
-- Структура таблицы `publication`
--

CREATE TABLE `publication` (
  `id` int(12) NOT NULL,
  `type` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `introduction` varchar(500) NOT NULL,
  `full_text` varchar(255) NOT NULL,
  `author` varchar(255) DEFAULT NULL,
  `source` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `publication`
--

INSERT INTO `publication` (`id`, `type`, `title`, `introduction`, `full_text`, `author`, `source`) VALUES
(3, 'article', 'Два письма и один мини-спектакль', 'В среду, 24 мая, президент России Владимир Путин вручал в Кремле госнаграды. В основном в этот день были отмечены деятели искусства, некоторые из них воспользовались моментом и под прицелом десятков телекамер просили за коллегу — худрука «Гоголь-центра» Кирилла Серебренникова. Тех, кто привык оказываться под прицелом куда более опасного оружия, Владимир Путин наградил без прессы.', 'Text\\article1.txt', 'Татьяна Меликян', NULL),
(4, 'article', 'Как украинская \"Жанна д\'Арк\" стала \"агентом Кремля\"', '\"Народная героиня\", \"наколотая\", \"проект ФСБ\". \"Страна\" проследила как менялось отношение к Надежде Савченко за год после ее возвращения в Украину', 'Text\\article2.txt', 'Анастасия Рафал', NULL),
(5, 'news', 'Кортеж Петра Порошенко объезжает пробки по встречке', 'Президент Украины передвигается в кортеже из семи машин и нарушает ПДД', 'Text\\news1.txt', NULL, 'https://strana.ua/articles/private-life/72569-kortezh-poroshenko-obezzhaet-probki-po-vstrechke.html'),
(6, 'news', 'Мэр Днепра Филатов грозит УКРОПу хлопнуть дверью', 'Почему накалились отношения в руководстве партии Коломойского', 'Text\\news2.txt', NULL, 'https://strana.ua/articles/analysis/72662-mer-dnepra-filatov-grozit-ukrop-hlopnut-dveryu.html');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `publication`
--
ALTER TABLE `publication`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `publication`
--
ALTER TABLE `publication`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
